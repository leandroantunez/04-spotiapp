import { Component } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  newTracks: any[] = [];
  loading: boolean;
  error: boolean;
  errorMessage: string;

  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.error = false;
    this.errorMessage = '';
    this.spotify.getNewReleases()
    .subscribe((data: any) => {
      this.newTracks = data;
      this.loading = false;
    }, (serviceError) => {
      this.loading = false;
      this.error = true;
      this.errorMessage = serviceError.error.error.message;
    })
  }

}
